<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'project-pirago' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~l9-u5dpa$w@ck$m($qbf>pJ?&bpxl%~EE>}#yDM[{hh[;18d}RW1orU0n V22@7' );
define( 'SECURE_AUTH_KEY',  'fscirEoet`fVNdAy36iU%&H61R+a:=(T4|V?W5^}%#8]ZJ,Bwz;X`;JQye4FH=s?' );
define( 'LOGGED_IN_KEY',    'd0W.bkg0I*|8^Ch6i2qDl4|%[cnFq43XMV>&;aaT~_y}~wL.t4i]C#op%A1O3/Y$' );
define( 'NONCE_KEY',        '&]LzW3(G8RazR G4V-B`BiB3hV&7vSk>[`]|Rk%7ruom-?`~|dk#Et_).(|ywz8P' );
define( 'AUTH_SALT',        'LW1a`ma.fP_|r=X%q&EkY7{f3sWJGTeO$m!8y/WbP/^+E9 %@/kI0h-[4kw_?><[' );
define( 'SECURE_AUTH_SALT', 'o?=.eLAjiE+>f&yk6uXq)w>})ZC-Y7E?sUZjh,* FAIMv(srC:dt2C[X$XP0wb,Y' );
define( 'LOGGED_IN_SALT',   'nxB|r?[}vGgQ,Hw40[]YzaO1wx@ZqAOFf+!zsO-aZD,Yh2JWDU0d&zk79fQ%Z}>[' );
define( 'NONCE_SALT',       'Tagx|/0MN@>>UYc|6pZGL<B$YCQyG=w*G1MF0GmhgZ[Gh_e}fD#~LG[lZ%yKY4ve' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
